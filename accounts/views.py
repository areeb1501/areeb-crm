from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.forms import inlineformset_factory
from .models import *
from .forms import Orderform, CreateUserForm, CustomerForm
from .filters import OrderFilter
from django.forms import formset_factory
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .decorators import unauthenticated_user, allowed_users, admin_only
from django.contrib.auth.models import Group
from django.core.mail import EmailMessage
from django.http import FileResponse
from reportlab.pdfgen import canvas
import reportlab


# Create your views here.
@unauthenticated_user
def registerpage(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()

            username = form.cleaned_data.get('username')
            email_id_to_send = form.cleaned_data.get('email')

            messages.success(request, 'Account was created for ' + username)
            email = EmailMessage(
                'wElcome to ELGM',
                'thank you, ' + username + '\n for creating your new account'
                                           '\n\n this is an automatically generated email',

                '<Areeb>',
                [email_id_to_send],
                ['demoareeb@gmail.com'],
                reply_to=['demoareeb@gmail.com'],
                headers={'Message-ID': 'foo'},
            )
            # email.attach_file('/images/')
            # print('attached')
            email.send(fail_silently=False)
            print("email sent")
            return redirect('login')

    context = {'form': form}
    return render(request, 'accounts/register.html', context)


@unauthenticated_user
def loginpage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            messages.info(request, 'USERNAME OR PASSWORD IS INCORRECT')

    context = {}
    return render(request, 'accounts/login.html', context)


def logoutuser(request):
    logout(request)
    return redirect('login')


@login_required(login_url='login')
@allowed_users(allowed_roles=['customer'])
def userPage(request):
    orders = request.user.customer.order_set.all()
    total_orders = orders.count()
    delivered = orders.filter(status='Delivered').count()
    pending = orders.filter(status='Pending').count()
    # print('orders',orders)
    context = {'orders': orders, 'total_orders': total_orders, 'delivered': delivered, 'pending': pending}
    return render(request, 'accounts/user.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['customer'])
def accountSettings(request):
    customer = request.user.customer  # request.user passes out the current user at any point of the time
    form = CustomerForm(instance=customer)

    if request.method == 'POST':
        form = CustomerForm(request.POST, request.FILES, instance=customer)
        if form.is_valid():
            form.save()
            # return redirect('/')

    context = {'form': form}
    return render(request, 'accounts/account_settings.html', context)


@login_required(login_url='login')
@admin_only
def home(request):
    orders = Order.objects.all()
    customers = Customer.objects.all()
    total_customers = customers.count()

    total_orders = orders.count()
    delivered = orders.filter(status='Delivered').count()
    pending = orders.filter(status='Pending').count()

    context = {'orders': orders, 'customers': customers,
               'total_orders': total_orders, 'delivered': delivered, 'pending': pending}

    return render(request, 'accounts/dashboard.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def products(request):
    products = Product.objects.all()
    return render(request, 'accounts/products.html', {'products': products})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'customer'])
def customer(request, pk_test):
    customer = Customer.objects.get(id=pk_test)

    orders = customer.order_set.all()
    order_count = orders.count()

    myFilter = OrderFilter(request.GET, queryset=orders)
    orders = myFilter.qs

    context = {'customer': customer, 'orders': orders, 'order_count': order_count, 'myFilter': myFilter}
    return render(request, 'accounts/customer.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def createOrder(request, pk):
    OrderFormSet = inlineformset_factory(Customer, Order, fields=('product', 'status'), extra=10)
    customer = Customer.objects.get(id=pk)
    formset = OrderFormSet(queryset=Order.objects.none(), instance=customer)
    # form = OrderForm(initial={'customer':customer})
    if request.method == 'POST':
        print('Printing POST:', request.POST)
        # form = OrderForm(request.POST)
        formset = OrderFormSet(request.POST, instance=customer)
        if formset.is_valid():
            formset.save()
            return redirect('/')

    context = {'form': formset}
    return render(request, 'accounts/order_form.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def updateOrder(request, pkt):
    order = Order.objects.get(id=pkt)
    form = Orderform(instance=order)
    print('ORDER:', order)
    if request.method == 'POST':
        form = Orderform(request.POST, instance=order)
        if form.is_valid():
            form.save()
            return redirect('/')

    context = {'form': form}
    return render(request, 'accounts/order_form.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def deleteOrder(request, pk):
    order = Order.objects.get(id=pk)

    if request.method == 'POST':
        order.delete()
        return redirect('/')

    context = {'item': order}
    return render(request, 'accounts/delete.html', context)


def createOrderInside(request, pk):
    order = Order.objects.get(id=pk)
    form = Orderform(instance=order)

    if request.method == 'POST':
        form = Orderform(request.POST, instance=order)
        if form.is_valid():
            form.save()
            return redirect('/')

    context = {'form': form}
    return render(request, 'accounts/order_form.html', context)


def update(request):
    return HttpResponse('Please login to user account view this page')


def team(request):
    teams = Team.objects.all()
    no_teams = teams.count()
    closed = teams.filter(status='Closed').count()

    members = TMember.objects.all()
    total_members = members.count()
    pending = teams.filter(status='Pending').count()
    context = {'no_teams': no_teams, 'closed': closed,
               'pending': pending, 'total_members': total_members,
               'teams': teams, 'members': members}
    # 'total_members': }

    return render(request, 'accounts/team-dashboard.html', context)


def category(request):
    return render(request, 'accounts/category.html')


def teamPage(request, pk_test):
    teams = Team.objects.get(id=pk_test)
    # orders = Team.order_set.all()
    # order_count = orders.count()

    context = {
        'teams': teams,
    }
    return render(request, 'accounts/team.html', context)

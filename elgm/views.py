from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.forms import inlineformset_factory
from .models import *
from .forms import ContactUsForm
from django.forms import formset_factory
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import Group
from django.core.mail import EmailMessage
from django.http import FileResponse
from reportlab.pdfgen import canvas
import reportlab


def home(request):
    return render(request, 'elgm/home.html')

def logon(request):
    return render(request, 'accounts/login.html')

def contact_us(request):
    form = ContactUsForm()
    if request.method == 'POST':
        form = ContactUsForm(request.POST)
        if form.is_valid():
            form.save()

    messages.success(request, 'Successfully submitted')
    context = {'form': form}
    return render(request, 'elgm/contact_us_now.html', context)
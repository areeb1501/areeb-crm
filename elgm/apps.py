from django.apps import AppConfig


class ElgmConfig(AppConfig):
    name = 'elgm'

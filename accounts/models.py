from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import pre_save


# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=200, null=True, blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    phone = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.first_name


def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
        print('profile created')


def update_profile(sender, instance, created, **kwargs):
    if created == False:
        instance.profile.save()
        print('profile updated!')


class Customer(models.Model):
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=250, null=True)
    phone = models.CharField(max_length=250, null=True)
    email = models.CharField(max_length=250, null=True)
    profile_pic = models.ImageField(default='profile.png', null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=250, null=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    CATEGORY = (
        ('indoor', 'indoor'),
        ('Outdoor', 'Outdoor'),
    )

    name = models.CharField(max_length=250, null=True)
    price = models.FloatField(null=True)
    category = models.CharField(max_length=250, null=True, choices=CATEGORY)
    decription = models.CharField(max_length=250, null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=250, null=True)

    def __str__(self):
        return self.name



class Team(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('Ongoing', 'Ongoing'),
        ('Closed', 'Closed'),
    )

    name = models.CharField(max_length=250, null=True)
    category = models.ManyToManyField(Category)
    orders_recieved = models.IntegerField( blank=True, null=True)
    status = models.CharField(max_length=250, null=True, choices=STATUS)
    note = models.CharField(max_length=1000, null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.name


class Order(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('Out For Delivery', 'Out For Delivery'),
        ('Delivered', 'Delivered'),
    )

    customer = models.ForeignKey(Customer, null=True, on_delete=models.SET_NULL)
    product = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL, blank=True)

    date_created = models.DateTimeField(auto_now_add=True, null=True)
    status = models.CharField(max_length=250, null=True, choices=STATUS)
    note = models.CharField(max_length=1000, null=True, blank=True)

    def __str__(self):
        return self.product.name


class TMember(models.Model):
    member = models.CharField(max_length=250, null=True)
    Team = models.ForeignKey(Team, null=True, on_delete=models.SET_NULL)
    date_created = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.member

'''
class customers(models.Model):
    customer = models.ForeignKey(Customer, null=True, on_delete=models.SET_NULL)
    category = models.ForeignKey(Category, null=True, on_delete=models.SET_NULL)
    team = models.ForeignKey(Team, null=True, on_delete=models.SET_NULL, blank=True)

    def __str__(self):
        return self.customer
'''

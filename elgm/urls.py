from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('',views.home,name='homee'),
    path('logon/',views.logon,name='logon'),
    path('contact-us/',views.contact_us,name='contact-us'),
]
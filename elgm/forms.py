from django.forms import ModelForm
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django import forms


class ContactUsForm(ModelForm):
    class Meta:
        model = ContactUs
        fields = ['name', 'email', 'subject', 'your_message']
